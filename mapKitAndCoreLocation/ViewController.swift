//
//  ViewController.swift
//  mapKitAndCoreLocation
//
//  Created by Admin on 20.07.16.
//  Copyright © 2016 Blazee. All rights reserved.
//

import UIKit
import MapKit


class ViewController: UIViewController, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var mapTypeSegmentedControl: UISegmentedControl!
    
    var annotation = MKPointAnnotation()
    
    lazy var geoCoder: CLGeocoder = CLGeocoder()

    var locationManager: CLLocationManager!
    
    var location: CLLocation!
    
    var locationString = ""
    
    @IBAction func changeMapType(sender: UISegmentedControl) {
        switch mapTypeSegmentedControl.selectedSegmentIndex {
        case 0:
            mapView.mapType = .Standard
        case 1:
            mapView.mapType = .Hybrid
        case 2:
            mapView.mapType = .Satellite
        default:
            mapView.mapType = .Standard
        }
    }
    
    @IBAction func actionSheet(sender: AnyObject) {
        let actionSheet = UIAlertController(title: "", message: "", preferredStyle: .ActionSheet)
        let toMyLoactionAction = UIAlertAction(title: "To my location", style: .Default) { (action) in
            self.locationManager.startUpdatingLocation()
            if self.location != nil {
                self.geoCoder.reverseGeocodeLocation(self.location) { (placemarks, error) in
                    if error == nil {
                        if let placemarks = placemarks {
                            let placemark = placemarks.last
                            self.annotation.title = "Country: \(placemark!.country!)"
                            self.annotation.subtitle = "Hello to \(placemark!.locality!)"
                            
                            self.annotation.coordinate = self.location.coordinate
                            self.mapView.addAnnotation(self.annotation)
                            self.mapView.selectAnnotation(self.annotation, animated: true)
                        }
                    } else {
                        self.alert("Something wrong with GeoCoder")
                    }
                }
            }
           
        }
        
        let encodeLoactionAction = UIAlertAction(title: "Encode location", style: .Default) { (action) in
            let alert = UIAlertController(title: "Fill location", message: "", preferredStyle: .Alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            let encodeAction = UIAlertAction(title: "Find location", style: .Default, handler: { (action) in
                
                self.geoCoder.geocodeAddressString(alert.textFields![0].text!, completionHandler: { (placemarks, error) in
                    if error == nil {
                        if let placemarks = placemarks {
                            let placemark = placemarks[0]
                            self.annotation.coordinate = placemark.location!.coordinate
                            self.annotation.title = "Country: \(placemark.country!)"
                            self.annotation.subtitle = "Hello to \(placemark.locality!)"
                            
                            self.mapView.addAnnotation(self.annotation)
                            self.mapView.selectAnnotation(self.annotation, animated: true)
                        }
                    } else {
                        self.alert(error!.localizedDescription)
                    }
                })
            })
            alert.addTextFieldWithConfigurationHandler({ (textField) in
                 textField.placeholder = "String location"
            })
            alert.addAction(cancelAction)
            alert.addAction(encodeAction)
            
            self.presentViewController(alert, animated: true, completion: nil)

            
            
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        
       
        
        actionSheet.addAction(toMyLoactionAction)
        actionSheet.addAction(cancelAction)
        actionSheet.addAction(encodeLoactionAction)
        
        presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        checkCoreLocationPermission()
    }
    
 
    
    func checkCoreLocationPermission() {
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
            locationManager.startUpdatingLocation()
        } else if CLLocationManager.authorizationStatus() == .NotDetermined {
            locationManager.requestWhenInUseAuthorization()
        } else if CLLocationManager.authorizationStatus() == .Restricted {
            alert("restricted") }
    }
    

    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        location = locations.last
       
    }
    
    func alert(error: String) {
        let alert = UIAlertController(title: "Error", message: error, preferredStyle: .Alert)
        let action = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alert.addAction(action)
        
        presentViewController(alert, animated: true, completion: nil)
        
    }



}

